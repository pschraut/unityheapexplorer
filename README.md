## Introduction
Heap Explorer is a memory profiler, debugger and analyzer for [Unity](http://unity3d.com).

This repository contains Heap Explorer for Unity 2017.4 - 2019.2. If you're looking for a newer version that works with Unity 2019.3, please visit the new Heap Explorer repository at https://github.com/pschraut/UnityHeapExplorer

## Known Issues
I tested Heap Explorer with Unity 2017.4 LTS. Earlier versions are not supported on purpose. Capturing a memory snapshot with Unity 2017.4 and Scripting Runtime .NET 4.x does not work, see [here](https://forum.unity.com/threads/wip-heap-explorer-memory-profiler-debugger-and-analyzer-for-unity.527949/page-2#post-3679282) for a workaround.

## Video

[https://www.youtube.com/watch?v=tcTl_7y8JBA](https://www.youtube.com/watch?v=tcTl_7y8JBA)

## Forum discussion

[https://forum.unity.com/threads/wip-heap-explorer-memory-profiler-debugger-and-analyzer-for-unity.527949/](https://forum.unity.com/threads/wip-heap-explorer-memory-profiler-debugger-and-analyzer-for-unity.527949/)

## Documentation
[http://www.console-dev.de/bin/HeapExplorer.pdf](http://www.console-dev.de/bin/HeapExplorer.pdf)
