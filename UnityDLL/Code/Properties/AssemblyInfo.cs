﻿//
// Heap Explorer for Unity. Copyright (c) 2019 Peter Schraut (www.console-dev.de). See LICENSE.md
// https://bitbucket.org/pschraut/unityheapexplorer/
//
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

[assembly: AssemblyIsEditorAssembly]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Peter Schraut")]
[assembly: AssemblyProduct("HeapExplorer")]
[assembly: AssemblyCopyright("Copyright 2017-2019 Peter Schraut")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyTitle("Heap Explorer")]
[assembly: Guid("7a990e84-7c9e-495a-a6ce-00ad90e66f32")]

